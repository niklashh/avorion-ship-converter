use avorion_ship_converter::{convert, logger::CONSOLE_LOGGER, ShipDesign};

use serde_xml_rs::from_reader;
use std::fs::File;

fn main() {
    log::set_logger(&CONSOLE_LOGGER)
        .map(|()| log::set_max_level(log::LevelFilter::Debug))
        .unwrap();

    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        panic!("File path for the plan file is required!")
    };
    let filename = &args[1];
    let contents = File::open(filename).expect(&format!("Unable to read {}", filename));

    let ship: ShipDesign = from_reader(contents).unwrap();

    let scad_file = convert::convert(&ship);

    scad_file.write_to_file(String::from("out.scad"));
}
