use crate::{convert, ShipDesign};
use log::{Level, Metadata, Record};
use serde_xml_rs::from_str;
use std::sync::Once;
use wasm_bindgen::prelude::*;
use web_sys::console::{error_1, log_1};

static SET_LOGGER_ONCE: Once = Once::new();

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

fn set_logger_once() {
    SET_LOGGER_ONCE.call_once(|| {
        if let Err(e) =
            log::set_logger(&JS_LOGGER).map(|()| log::set_max_level(log::LevelFilter::Info))
        {
            error_1(&format!("setting logger failed: {}", e).into());
        }
    })
}

fn init() {
    console_error_panic_hook::set_once();
    set_logger_once();
}

#[wasm_bindgen]
pub fn parse_ship(s: &str) -> String {
    init();

    let ship: ShipDesign = from_str(s).unwrap();
    convert::convert(&ship).get_code()
}

pub static JS_LOGGER: JSLogger = JSLogger;
pub struct JSLogger;

impl log::Log for JSLogger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        match record.level() {
            Level::Error => error_1(&format!("{}", record.args()).into()),
            _ => log_1(&format!("{}", record.args()).into()),
        }
    }

    fn flush(&self) {}
}
