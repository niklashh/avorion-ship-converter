pub fn look_vector(look: &String, up: &String) -> Vec<i32> {
    if look == "0" && up == "2" {
        vec![0, 90, 180]
    } else if look == "0" && up == "3" {
        vec![-90, -90, 90]
    } else if look == "0" && up == "4" {
        vec![-90, 0, 90]
    } else if look == "0" && up == "5" {
        vec![90, 0, -90]
    } else if look == "1" && up == "2" {
        vec![0, -90, 180]
    } else if look == "1" && up == "3" {
        vec![0, 90, 0]
    } else if look == "1" && up == "4" {
        vec![90, 180, 90]
    } else if look == "1" && up == "5" {
        vec![90, 0, 90]
    } else if look == "2" && up == "0" {
        vec![0, -90, 90]
    } else if look == "2" && up == "1" {
        vec![0, 90, -90]
    } else if look == "2" && up == "4" {
        vec![-90, 0, 180]
    } else if look == "2" && up == "5" {
        vec![90, 0, 0]
    } else if look == "3" && up == "0" {
        vec![-90, 90, 0]
    } else if look == "3" && up == "1" {
        vec![0, -90, -90]
    } else if look == "3" && up == "4" {
        vec![-90, 0, 0]
    } else if look == "3" && up == "5" {
        vec![90, 0, 180]
    } else if look == "4" && up == "0" {
        vec![0, 180, 90]
    } else if look == "4" && up == "1" {
        vec![0, 180, -90]
    } else if look == "4" && up == "2" {
        vec![0, 180, 180]
    } else if look == "4" && up == "3" {
        vec![180, 0, 180]
    } else if look == "5" && up == "0" {
        vec![0, 0, 90]
    } else if look == "5" && up == "1" {
        vec![0, 0, -90]
    } else if look == "5" && up == "2" {
        vec![180, 180, 0]
    } else if look == "5" && up == "3" {
        vec![0, 0, 0]
    } else {
        panic!("invalid look and up")
    }
}
