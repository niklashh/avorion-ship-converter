use crate::{utils::look_vector, ShipDesign};
use log::{info, warn};
use scad::{
    scad, vec3, Color, Cube, Polyhedron, Rotate, ScadFile, ScadObject, Scale, Translate, Union,
};
use std::collections::HashMap;

const ROUND_PRECISION: f32 = 10_000.0; // used for rounding the floating point numbers from the xml

pub fn convert(ship: &ShipDesign) -> ScadFile {
    let mut scad_file = ScadFile::new();
    let mut parent = scad!(Union);

    let edge = scad!(Polyhedron(
        vec![
            vec3(0.0, 0.0, 0.0),
            vec3(1.0, 0.0, 0.0),
            vec3(1.0, 0.0, 1.0),
            vec3(0.0, 0.0, 1.0),
            vec3(0.0, 1.0, 1.0),
            vec3(1.0, 1.0, 1.0)
        ],
        vec![
            vec![0, 1, 2, 3],
            vec![5, 4, 3, 2],
            vec![0, 4, 5, 1],
            vec![0, 3, 4],
            vec![5, 2, 1]
        ]
    ));

    let corner = scad!(Polyhedron(
        vec![
            vec3(0.0, 0.0, 1.0),
            vec3(1.0, 0.0, 0.0),
            vec3(1.0, 0.0, 1.0),
            vec3(1.0, 1.0, 1.0),
        ],
        vec![vec![0, 2, 1], vec![0, 3, 2], vec![1, 2, 3], vec![0, 1, 3],]
    ));

    let inner_corner = scad!(Polyhedron(
        vec![
            vec3(1.0, 0.0, 1.0),
            vec3(0.0, 0.0, 1.0),
            vec3(1.0, 0.0, 0.0),
            vec3(0.0, 0.0, 0.0),
            vec3(1.0, 1.0, 1.0),
            vec3(0.0, 1.0, 1.0),
            vec3(1.0, 1.0, 0.0),
        ],
        vec![
            vec![0, 2, 3, 1],
            vec![0, 4, 6, 2],
            vec![0, 1, 5, 4],
            vec![3, 2, 6],
            vec![4, 5, 6],
            vec![1, 3, 5],
            vec![3, 6, 5],
        ]
    ));

    let outer_corner = scad!(Polyhedron(
        vec![
            vec3(0.0, 0.0, 1.0),
            vec3(1.0, 0.0, 0.0),
            vec3(1.0, 0.0, 1.0),
            vec3(1.0, 1.0, 1.0),
            vec3(0.0, 0.0, 0.0)
        ],
        vec![
            vec![0, 2, 1, 4],
            vec![0, 3, 2],
            vec![1, 2, 3],
            vec![1, 3, 4],
            vec![0, 4, 3]
        ]
    ));

    let twisted_corner1 = scad!(Polyhedron(
        vec![
            vec3(0.0, 0.0, 1.0),
            vec3(1.0, 0.0, 0.0),
            vec3(1.0, 1.0, 0.0),
            vec3(0.0, 0.0, 0.0),
        ],
        vec![vec![0, 2, 1], vec![0, 3, 2], vec![0, 1, 3], vec![1, 2, 3]]
    ));

    let twisted_corner2 = scad!(Polyhedron(
        vec![
            vec3(0.0, 0.0, 1.0),
            vec3(1.0, 0.0, 1.0),
            vec3(1.0, 1.0, 1.0),
            vec3(0.0, 0.0, 0.0),
        ],
        vec![vec![0, 2, 1], vec![0, 3, 2], vec![0, 1, 3], vec![1, 2, 3]]
    ));

    let flat_corner = scad!(Polyhedron(
        vec![
            vec3(0.0, 0.0, 1.0),
            vec3(1.0, 0.0, 0.0),
            vec3(1.0, 0.0, 1.0),
            vec3(0.0, 1.0, 1.0),
            vec3(1.0, 1.0, 0.0),
        ],
        vec![
            vec![0, 2, 1],
            vec![0, 3, 2],
            vec![1, 2, 4],
            vec![2, 3, 4],
            vec![0, 1, 4, 3]
        ]
    ));

    let cube = scad!(Cube(vec3(1.0, 1.0, 1.0)));

    info!("Number of blocks: {}", ship.plan.items.len());
    let mut analytics: HashMap<String, u32> = HashMap::new();
    for item in &ship.plan.items {
        let b = &item.block;
        let lx = (b.lx * ROUND_PRECISION).floor() / ROUND_PRECISION;
        let ly = (b.ly * ROUND_PRECISION).floor() / ROUND_PRECISION;
        let lz = (b.lz * ROUND_PRECISION).floor() / ROUND_PRECISION;
        let ux = (b.ux * ROUND_PRECISION).ceil() / ROUND_PRECISION;
        let uy = (b.uy * ROUND_PRECISION).ceil() / ROUND_PRECISION;
        let uz = (b.uz * ROUND_PRECISION).ceil() / ROUND_PRECISION;
        let sx = ((ux - lx) * ROUND_PRECISION).ceil() / ROUND_PRECISION;
        let sy = ((uy - ly) * ROUND_PRECISION).ceil() / ROUND_PRECISION;
        let sz = ((uz - lz) * ROUND_PRECISION).ceil() / ROUND_PRECISION;
        let scaled_cube = scad!(Scale(vec3(sx,sy,sz)); cube.clone());
        let cube = scad!(Translate(vec3(lx,ly,lz)); scaled_cube);

        if let Some(a) = analytics.get_mut(&b.index[..]) {
            *a += 1;
        } else {
            analytics.insert(b.index.clone(), 1);
        }

        match &b.index[..] {
            "21" | // turret base edge
            "100" | // edge
            "101" | // corner
            "102" | // inverted corner
            "103" | // centered corner
            "104" | // armour edge
            "105" | // armour corner
            "106" | // armour inverted corner
            "107" | // armour centered corner
            "108" | // twisted corner 1
            "109" | // twisted corner 2
            "112" | // flat corner
            "151" | // glow edge
            "152" | // glow corner
            "153" | // glow inverted corner
            "154" | // glow centered corner
            "171" | // glass edge
            "172" | // glass corner
            "173" | // glass inverted corner
            "174" | // glass centered corner
            "185" | // stone edge
            "186" | // stone corner
            "187" | // stone inverted corner
            "188" | // stone centered corner
            "191" | // hologram edge
            "182" | // hologram corner
            "183" | // hologram inverted corner
            "184" | // hologram centered corner
            "511" | // rich stone edge
            "512" | // rich stone corner
            "513" | // rich stone inverted corner
            "514" // rich stone centered corner
                => {
                let color = match b.up.as_str() {
                    "1" => vec3(1.0, 0.0, 0.0),
                    "2" => vec3(1.0, 1.0, 0.0),
                    "3" => vec3(0.0, 1.0, 0.0),
                    "4" => vec3(0.0, 1.0, 1.0),
                    "5" => vec3(0.0, 0.0, 1.0),
                    _ => vec3(0.0, 0.0, 0.0),
                };
                let lookvec = look_vector(&b.look, &b.up);

                let base = match &b.index[..] {
                    "21" | "100" | "104" | "151" | "171" | "185" | "191" | "511" => edge.clone(),
                    "101" | "105" | "152" | "172" | "186" | "182" | "512" => corner.clone(),
                    "102" | "106" | "153" | "173" | "187" | "183" | "513" => inner_corner.clone(),
                    "103" | "107" | "154" | "174" | "188" | "184" | "514" => outer_corner.clone(),
                    "108" => twisted_corner1.clone(),
                    "109" => twisted_corner2.clone(),
                    "112" => flat_corner.clone(),
                    // "105" => default_edge.clone(), // armor
                    // "107" => outer_corner.clone(), // armor
                    _ => unreachable!(),
                };

                // move center to origin
                let edge = scad!(Translate(vec3(-0.5, -0.5, -0.5)); base);

                // rotate around origin
                let edge = scad!(Rotate(lookvec[0] as f32, vec3(1f32, 0f32, 0f32)); edge);
                let edge = scad!(Rotate(lookvec[1] as f32, vec3(0f32, 1f32, 0f32)); edge);
                let edge = scad!(Rotate(lookvec[2] as f32, vec3(0f32, 0f32, 1f32)); edge);

                // move back
                let edge = scad!(Translate(vec3(0.5, 0.5, 0.5)); edge);

                // scale to size
                let edge = scad!(Scale(vec3(sx, sy, sz)); edge.clone());

                // move to correct location
                let edge = scad!(Translate(vec3(lx, ly, lz)); edge);
                let edge = scad!(
                    Color(color); edge
                );
                parent.add_child(edge);
            }
            "1" | // hull
            "2" | // blank hull
            "3" | // engine
            "4" | // stone
            "5" | // cargo
            "6" | // crew quarters
            "7" | // thruster
            "8" | // armour
            "9" | // framework
            "10" | // hangar
            "11" | // dock
            "12" | // rotation lock
            "13" | // directional thruster
            "14" | // gyro array
            "15" | // inertia dampener
            "16" | // flight recorder
            "17" | // assembly
            "20" | // turret base
            "50" | // shield generator
            "51" | // battery
            "52" | // generator
            "53" | // integrity field generator
            "54" | // computer core
            "55" | // hyperspace core
            "56" | // transporter
            "57" | // academy
            "58" | // cloning pods
            "60" | // solar
            "61" | // light
            "150" | // glow
            "170" | // glass block
            "180" | // reflector
            "190" | // hologram
            "510" // rich stone
                => parent.add_child(cube),
            _ => {
                warn!("Unknown index: {}", &b.index[..]);
            },
        }
    }

    info!("Block index frequency: {:#?}", analytics);

    scad_file.add_object(parent);
    scad_file
}
