use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Block {
    pub lx: f32,
    pub ly: f32,
    pub lz: f32,
    pub ux: f32,
    pub uy: f32,
    pub uz: f32,
    pub index: String,
    pub look: String,
    pub up: String,
}

#[derive(Debug, Deserialize)]
pub struct Item {
    pub block: Block,
}

#[derive(Debug, Deserialize)]
pub struct Plan {
    #[serde(rename = "item", default)]
    pub items: Vec<Item>,
}

#[derive(Debug, Deserialize)]
pub struct ShipDesign {
    pub plan: Plan,
}

pub mod convert;
pub mod logger;
pub mod utils;

#[cfg(feature = "wasm")]
pub mod wasm;
