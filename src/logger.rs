use log::{Level, Metadata, Record};

pub static CONSOLE_LOGGER: ConsoleLogger = ConsoleLogger;

pub struct ConsoleLogger;

impl log::Log for ConsoleLogger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        use colored::*;
        if self.enabled(record.metadata()) {
            let color = match record.level() {
                Level::Debug => Color::Blue,
                Level::Info => Color::Green,
                Level::Warn => Color::Yellow,
                Level::Error => Color::Red,
                _ => Color::White,
            };
            println!("{} {}", record.level().as_str().color(color), record.args());
        }
    }

    fn flush(&self) {}
}
