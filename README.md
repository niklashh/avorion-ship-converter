<div align="center">

  <h1>avorion-ship-converter</h1>

  <sub>Built with 🦀</sub>
</div>

## About

Converter for [avorion](https://www.avorion.net/) ship XML
to [SCAD](https://openscad.org/) source code.

This converter does not create 3D-models. To get a 3D-model,
you need to render the scad file which might take some minutes.

## Usage

Try it [online](https://niklashh.gitlab.io/avorion-ship-converter)!

### Where to find the ship XML files?

`~/.avorion/ships` on linux.

### Running the crate

```shell
cargo run /path/to/ship.xml
```

The generated scad file is written to `./out.scad`.

### Bundling the website

You need [`wasm-pack`](https://github.com/rustwasm/wasm-pack)
and [`node.js`](https://nodejs.org/en/) to run the website.

Set up wasm as a compilation target with rustup:

```shell
rustup target add wasm32-unknown-unknown
```

Compile the wasm binary:

```shell
wasm-pack build -- --features wasm
```

> The generated wasm binary is in `/pkg`.

Now go to `/www` and install node dependencies:

```shell
npm install
```

Next bundle it with webpack to create a static page:

```shell
npm run build
```

> The bundle is created in `/www/dist`, open your browser at
> `file:///path/to/avorion-ship-converter/www/dist` to view the page

## Development

### Project structure

- `www`: the javascript source code for the website
- `src/wasm.rs`: the rust wasm bindings for using the converter
  from javascript

### `www`

Run the webpack dev server to watch for changes in the html,
javascript and wasm files:

```shell
npm run dev
```

> Doesn't automatically compile rust into wasm.

## Contributing

All contributions are welcome!

If you find any issues, please submit them in the
[issues](https://gitlab.com/niklashh/avorion-ship-converter/-/issues).

### Code of Conduct

https://www.rust-lang.org/policies/code-of-conduct
