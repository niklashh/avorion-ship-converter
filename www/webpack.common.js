const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  entry: {
    app: './src/bootstrap.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bootstrap.js',
  },
  plugins: [
    new CopyPlugin({ patterns: ['./src/index.html'] }),
  ],
  experiments: {
    syncWebAssembly: true,
  },
};
