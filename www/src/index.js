import * as wasm from 'avorion-ship-converter';

const convertButton = document.getElementById('convert');

convertButton.addEventListener('click', () => {
  convertButton.disabled = true;
  const input = document.getElementById('input');
  if (input.files.length !== 1) {
    console.error('no input file');
    return;
  }
  const filename = input.files[0].name;
  const reader = new FileReader();
  reader.readAsText(input.files[0]);
  reader.onload = () => {
    const ship = reader.result;
    try {
      const output = wasm.parse_ship(ship);
      const blob = new Blob([output], { type: 'text/plain' });
      const url = URL.createObjectURL(blob);
      const hiddenElement = document.createElement('a');

      hiddenElement.href = url;
      hiddenElement.target = '_blank';
      hiddenElement.download = `${filename.split('.').slice(0, -1).join('.')}.scad`;
      hiddenElement.click();
    } catch (error) {
      console.error(error);
      alert('converting failed');
      return;
    } finally {
      convertButton.disabled = false;
    }
  };
  reader.onerror = () => {
    console.error(reader.error);
    convertButton.disabled = false;
  };
});
